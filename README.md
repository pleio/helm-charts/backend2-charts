# Backend2 Charts

This repository will contain charts related to [Backend2](https://gitlab.com/pleio/backend2). For now only the `backend2-sites` chart is hosted in this repository. In the near future we'll also include the other charts.

## Backend2-sites (domains)
Domains that are routed to backend2 are managed through the websitesManual and websitesAuto values. Any domain in websitesAuto obtains automatic letsencrypt certificates. WebsitesManual provides a way to reference manually added certificates for example when a PKI certificate is required for a website or for a wildcard domain such as *.pleio.nl for which the letsencrypt certificate cannot yet be automated.
