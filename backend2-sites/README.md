# backend2-sites

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

Helm chart for deploying K8S Ingress of backend2 site domains

## Source Code

* <https://gitlab.com/pleio/helm-charts/backend2-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| adminDomain | string | `""` | Domain to use for the admin interface |
| annotations | object | `{"kubernetes.io/ingress.class":"nginx","nginx.ingress.kubernetes.io/configuration-snippet":"proxy_force_ranges \"On\";\n","nginx.ingress.kubernetes.io/modsecurity-snippet":"SecRuleEngine On\nSecRule REQUEST_URI \"@beginsWith /graphql\" \"id:1,ctl:ruleEngine=Off\"\nSecRule REQUEST_URI \"@beginsWith /superadmin\" \"id:2,ctl:ruleEngine=Off\"\nSecRuleRemoveById 950000-959999\n","nginx.ingress.kubernetes.io/proxy-body-size":"2050m"}` | Annotations to add to all ingresses (except for redirect ingresses) |
| autoAnnotations | object | `{"cert-manager.io/cluster-issuer":"override"}` | Annotations to add to the auto-ingresses (those that use LetsEncrypt) |
| manualAnnotations | object | `{}` | Annotations to add to the manual-ingresses (those that use their own certificates) |
| redirectAnnotations | object | `{}` | Annotations to add to the redirect ingress |
| redirectClusterIssuer | string | `"override"` | Cluster issuer to use for the redirect certificates |
| redirects | list | `[]` | Handles redirects (e.g. Pleio-subdomain to own (sub)domain) |
| serviceName | string | `"backend2-api"` |  |
| websitesAuto | list | `[]` | Websites that use LetsEncrypt certificates |
| websitesManual | list | `[]` | Websites that use their own certificates (secret must exist prior to adding the entry to this list) |
| websocketServiceName | string | `"backend2-websocket"` |  |
